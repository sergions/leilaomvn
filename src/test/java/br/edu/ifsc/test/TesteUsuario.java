package br.edu.ifsc.test;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TesteUsuario {

	@Test
	public void verificaCadastroDeNovoUsuario() {
		System.setProperty("webdriver.chrome.driver", "/home/sergio/opt/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("http://localhost:8080/leilaomvn");

		WebElement link = driver.findElement(By.linkText("Usuários"));
		link.click();
		link = driver.findElement(By.linkText("Novo Usuário"));
		link.click();

		WebElement nome = driver.findElement(By.name("usuario.nome"));
		nome.sendKeys("Nome do usuário");
		WebElement email = driver.findElement(By.name("usuario.email"));
		email.sendKeys("mail@mail");

		WebElement botao = driver.findElement(By.id("btnSalvar"));
		botao.click();
		
		Assert.assertTrue(driver.getPageSource().contains("Nome do usuário"));
		Assert.assertTrue(driver.getPageSource().contains("mail@mail"));

	}
}
